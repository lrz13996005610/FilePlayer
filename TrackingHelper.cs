using System.Threading;
using System.Windows;
using System.Windows.Controls;
using TouchFramework;
using TouchFramework.Tracking;

namespace FilePlayer
{
	internal class TrackingHelper
	{
		internal enum TrackingType
		{
			Mouse,
			TUIO,
			Traal
		}

		internal static FrameworkControl GetTracking(FrameworkElement uiParent, TrackingType trType)
		{
			switch (trType)
			{
			case TrackingType.Mouse:
				return GetMouseTracking(uiParent);
			case TrackingType.TUIO:
				return GetTuioTracking(uiParent);
			case TrackingType.Traal:
				return GetTraalTracking(uiParent);
			default:
				return GetMouseTracking(uiParent);
			}
		}

		internal static FrameworkControl GetMouseTracking(FrameworkElement uiParent)
		{
			FrameworkControl tmpFrame = new MouseTracking(uiParent);
			FrameworkConfiguration conf = GetMouseConfig(uiParent);
			tmpFrame.ConfigureFramework(conf);
			return tmpFrame;
		}

		internal static FrameworkControl GetTraalTracking(FrameworkElement uiParent)
		{
			FrameworkControl tmpFrame = new TraalTracking(uiParent);
			FrameworkConfiguration conf = GetTraalConfig(uiParent);
			tmpFrame.ConfigureFramework(conf);
			return tmpFrame;
		}

		internal static FrameworkControl GetTuioTracking(FrameworkElement uiParent)
		{
			FrameworkControl tmpFrame = new TuioTracking(uiParent);
			FrameworkConfiguration conf = GetTuioConfig(uiParent);
			tmpFrame.ConfigureFramework(conf);
			return tmpFrame;
		}

		internal static FrameworkConfiguration GetMouseConfig(FrameworkElement uiParent)
		{
			return new MouseConfiguration
			{
				Owner = uiParent,
				UIManagedThreadId = Thread.CurrentThread.ManagedThreadId,
				EventWindow = (Canvas)uiParent
			};
		}

		internal static FrameworkConfiguration GetTraalConfig(FrameworkElement uiParent)
		{
			AppConfig.LoadAppConfig();
			ProjectionConfig proj = ProjectionConfig.Load(AppConfig.TrackingPath);
			if (proj == null)
			{
				proj = ProjectionConfig.LoadDefaults();
			}
			AlignConfig align = new AlignConfig
			{
				FlipX = AppConfig.FlipX,
				FlipY = AppConfig.FlipY
			};
			return new TraalConfiguration
			{
				Owner = uiParent,
				UIManagedThreadId = Thread.CurrentThread.ManagedThreadId,
				CorrectProjection = AppConfig.CorrectProjection,
				UseVfwDriver = false,
				Alignment = align,
				Projection = proj,
				TrackingConfigPath = AppConfig.TrackingPath
			};
		}

		internal static FrameworkConfiguration GetTuioConfig(FrameworkElement uiParent)
		{
			AppConfig.LoadAppConfig();
			ProjectionConfig proj = ProjectionConfig.Load(AppConfig.TrackingPath);
			if (proj == null)
			{
				proj = ProjectionConfig.LoadDefaults();
			}
			AlignConfig align = new AlignConfig
			{
				FlipX = AppConfig.FlipX,
				FlipY = AppConfig.FlipY
			};
			return new TuioConfiguration
			{
				Owner = uiParent,
				UIManagedThreadId = Thread.CurrentThread.ManagedThreadId,
				CorrectProjection = AppConfig.CorrectProjection,
				Alignment = align,
				Projection = proj,
				Port = AppConfig.Port
			};
		}
	}
}
