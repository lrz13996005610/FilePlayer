using System.Configuration;

namespace FilePlayer
{
	internal static class AppConfig
	{
		private const bool DEFAULT_FLIPX = false;

		private const bool DEFAULT_FLIPY = false;

		private const bool DEFAULT_CP = false;

		private const int DEFAULT_PORT = 3333;

		private const bool DEFAULT_FS = false;

		internal static bool FlipX = false;

		internal static bool FlipY = false;

		internal static string TrackingPath = string.Empty;

		internal static bool CorrectProjection = false;

		internal static int Port = 3333;
		internal static int Width = 1920;
		internal static int Height = 1080;

		internal static bool StartFullscreen = false;

		internal static void LoadAppConfig()
		{
			TrackingPath = GetConfigVal("trackingpath");
			bool parseOk = false;
			bool fX = false;
			FlipX = (bool.TryParse(GetConfigVal("flipx"), out fX) && fX);
			bool fY = false;
			FlipY = (bool.TryParse(GetConfigVal("flipy"), out fY) && fY);
			bool cp = false;
			CorrectProjection = (bool.TryParse(GetConfigVal("correctprojection"), out cp) && cp);
			bool sf = false;
			StartFullscreen = (bool.TryParse(GetConfigVal("startfullscreen"), out sf) && sf);
			int port = 3333;
			Port = (int.TryParse(GetConfigVal("port"), out port) ? port : 3333);
			int width = 1920;
			Width = (int.TryParse(GetConfigVal("width"), out width) ? width : 1920);
			int height = 1080;
			Height = (int.TryParse(GetConfigVal("height"), out height) ? height : 1080);
		}

		private static string GetConfigVal(string name)
		{
			string data = ConfigurationManager.AppSettings[name];
			if (data == null)
			{
				return string.Empty;
			}
			return data;
		}
	}
}
