using NAudio.FileFormats.Mp3;
using NAudio.Wave;
using NLayer.NAudioSupport;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using TouchFramework;
using TouchFramework.ControlHandlers;
using TouchFramework.Events;
using TouchFramework.Tracking;

namespace FilePlayer
{
    public partial class Window1 : Window
    {
        private double screen_width = SystemParameters.PrimaryScreenWidth;

        private double screen_height = SystemParameters.PrimaryScreenHeight;

        private double window_width = 640.0;

        private double window_height = 480.0;

        private double window_left = 0.0;

        private double window_top = 0.0;

        private int numItems = 0;

        private const int MAX_ITEMS = 40;

        private string[] strPic = new string[6]
        {
            ".jpg",
            ".png",
            ".gif",
            ".tiff",
            ".bmp",
            ".jpeg"
        };

        private string[] strVideo = new string[5]
        {
            ".wmv",
            ".mpeg",
            ".mpg",
            ".avi",
            ".mp4"
        };

        private List<ItemData> lstItemData = new List<ItemData>();

        private DispatcherTimer timer;

        private int index = -1;

        private Dictionary<FrameworkElement, Storyboard> dicSb = new Dictionary<FrameworkElement, Storyboard>();

        private TrackingHelper.TrackingType currentTrackingType = TrackingHelper.TrackingType.TUIO;

        private bool fullscreen = false;

        private static Random randomGen = new Random();

        private Dictionary<int, UIElement> points = new Dictionary<int, UIElement>();

        private FrameworkControl framework = null;



        public Window1()
        {
            InitializeComponent();
            AppConfig.LoadAppConfig();
            this.Width = AppConfig.Width;
            this.Height = AppConfig.Height;
            this.Top = 0;
            this.Left = 0;
            EventManager.RegisterClassHandler(typeof(UserControl), MTEvents.TouchDownEvent, new RoutedEventHandler(OnTouchDownEvent));
            EventManager.RegisterClassHandler(typeof(UserControl), MTEvents.ResumeEvent, new RoutedEventHandler(OnResumeEvent));
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(App.Interval);
            timer.Tick += timer_Tick;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            framework = TrackingHelper.GetTracking(canvas1, currentTrackingType);
            FrameworkControl frameworkControl = framework;
            frameworkControl.OnProcessUpdates = (FrameworkControl.ProcessUpdatesDelegate)Delegate.Combine(frameworkControl.OnProcessUpdates, new FrameworkControl.ProcessUpdatesDelegate(DisplayPoints));
            framework.Start();
            if (AppConfig.StartFullscreen)
            {
                toggleFullscreen();
            }
            else
            {
                //double resolutionX = SystemParameters.PrimaryScreenWidth;
                //double resolutionY = SystemParameters.PrimaryScreenHeight;
                //switchFullScreen(resolutionX, resolutionY);
            }
            takeBackground();
            LoadFolders();
            PosAll();
            timer.Start();
            Task.Factory.StartNew(PlayOrStopMusic);
        }

        //private void PlayMusic()
        //{
        //    try
        //    {
        //        MediaPlayer player = new MediaPlayer();
        //        player.MediaEnded += (sender, e) =>
        //        {
        //            //播放结束后 又重新播放
        //            player.Position = new TimeSpan(0);
        //        };
        //        player.Open(new Uri("bg.mp3", UriKind.Relative));
        //        player.Volume = 50;
        //        player.Play();
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        private WaveOut waveOut;
        private void PlayOrStopMusic()
        {
            try
            {
                //var waveOut = new WaveOut();
                //var fileName = "bg.mp3";
                //var builder = new Mp3FileReader.FrameDecompressorBuilder(wf => new Mp3FrameDecompressor(wf));
                //var reader = new Mp3FileReader(fileName, builder);
                //LoopStream loop = new LoopStream(reader);
                //waveOut.Init(loop);
                //waveOut.PlaybackStopped += (s, e) =>
                //{
                //    waveOut.Stop();
                //    waveOut.Play();
                //};
                //waveOut.Play();


                if (waveOut == null)
                {
                    var builder = new Mp3FileReader.FrameDecompressorBuilder(wf => new Mp3FrameDecompressor(wf));
                    var reader = new Mp3FileReader("bg.mp3", builder);
                    LoopStream loop = new LoopStream(reader);
                    waveOut = new WaveOut();
                    waveOut.Init(loop);
                    waveOut.Play();
                }
                else
                {
                    waveOut.Stop();
                    waveOut.Dispose();
                    waveOut = null;
                }
            }
            catch (Exception ex)
            {
            }
        }



        private void DisplayPoints()
        {
            foreach (int i in points.Keys)
            {
                if (!framework.AllTouches.Keys.Contains(i))
                {
                    canvas1.Children.Remove(points[i]);
                }
            }
            foreach (TouchFramework.Touch te in framework.AllTouches.Values)
            {
                DisplayPoint(te.TouchId, te.TouchPoint);
            }
        }

        private void RemovePoints()
        {
            foreach (UIElement e in points.Values)
            {
                canvas1.Children.Remove(e);
            }
            points = new Dictionary<int, UIElement>();
        }

        private void DisplayPoint(int id, PointF p)
        {
            DisplayPoint(id, p, Colors.White);
        }

        private void DisplayPoint(int id, PointF p, System.Windows.Media.Color brushColor)
        {
            Ellipse e = null;
            if (points.ContainsKey(id))
            {
                e = (points[id] as Ellipse);
                e.RenderTransform = new TranslateTransform(p.X - 13f, p.Y - 13f);
            }
            if (e == null)
            {
                e = new Ellipse();
                RadialGradientBrush radialGradient = new RadialGradientBrush();
                radialGradient.GradientOrigin = new System.Windows.Point(0.5, 0.5);
                radialGradient.Center = new System.Windows.Point(0.5, 0.5);
                radialGradient.RadiusX = 0.5;
                radialGradient.RadiusY = 0.5;
                System.Windows.Media.Color shadow = Colors.Black;
                shadow.A = 30;
                radialGradient.GradientStops.Add(new GradientStop(shadow, 0.9));
                brushColor.A = 60;
                radialGradient.GradientStops.Add(new GradientStop(brushColor, 0.8));
                brushColor.A = 150;
                radialGradient.GradientStops.Add(new GradientStop(brushColor, 0.1));
                radialGradient.Freeze();
                e.Height = 26.0;
                e.Width = 26.0;
                e.Fill = radialGradient;
                int eZ = framework.MaxZIndex + 100;
                e.IsHitTestVisible = false;
                e.RenderTransform = new TranslateTransform(p.X - 13f, p.Y - 13f);
                canvas1.Children.Add(e);
                Panel.SetZIndex(e, eZ);
                points.Add(id, e);
            }
        }

        private void LoadFolders()
        {
            if (lstItemData == null)
            {
                lstItemData = new List<ItemData>();
            }
            try
            {
                string[] files = null;
                files = Directory.GetFiles(App.ImgDirectory, "*", SearchOption.AllDirectories);
                lstItemData.Clear();
                for (int i = 0; i < files.Length; i++)
                {
                    string file = files[i];
                    if (!Directory.Exists(System.IO.Path.GetFullPath(file).Replace(System.IO.Path.GetExtension(file), "")) && !file.Contains("bg."))
                    {
                        lstItemData.Add(new ItemData
                        {
                            Id = i,
                            Name = System.IO.Path.GetFullPath(file),
                            FileType = ((!strPic.Contains(System.IO.Path.GetExtension(file).ToLower())) ? 1 : 0)
                        });
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            index++;
            index = index % lstItemData.Count;
            if (index < lstItemData.Count)
            {
                var item = lstItemData[index];
                try
                {
                    FrameworkElement fe = Create(lstItemData[index]);
                    int y = randomGen.Next(80, Math.Max(200, (int)canvas1.ActualHeight - (int)fe.Height - 80));
                    Canvas.SetTop(fe, y);
                    Run(fe);
                }
                catch (Exception)
                {

                }

            }
        }

        private FrameworkElement Create(ItemData item)
        {
            FrameworkElement fe = (item.FileType != 0) ? AddVideo(item.Name) : AddPhoto(item.Name);
            canvas1.Children.Add(fe);
            return fe;
        }

        private void OnTouchDownEvent(object sender, RoutedEventArgs e)
        {
            FrameworkElement fe = sender as FrameworkElement;
            if (dicSb.ContainsKey(fe))
            {
                Storyboard sbRun = dicSb[fe];
                sbRun.Pause(fe);
            }
        }

        private void OnResumeEvent(object sender, RoutedEventArgs e)
        {
            FrameworkElement fe = sender as FrameworkElement;
            if (dicSb.ContainsKey(fe))
            {
                Storyboard sbRun = dicSb[fe];
                if (sbRun.GetIsPaused(fe))
                {
                    sbRun.Resume(fe);
                }
            }
        }

        private void Run(FrameworkElement fe)
        {
            //Task.Factory.StartNew(() =>
            //{
            //    Thread.Sleep(200);
            //    this.Dispatcher.Invoke(new Action(() =>
            //    {
            //        canvas1.Children.Remove(fe);
            //        GC.Collect();
            //    }), DispatcherPriority.Render);

            //});

            //return;
            Storyboard sbRun = (TryFindResource("sbRun") as Storyboard).Clone();
            DoubleAnimationUsingKeyFrames dakf = sbRun.Children[0] as DoubleAnimationUsingKeyFrames;
            dakf.KeyFrames[0].Value = 0;
            dakf.KeyFrames[1].Value = canvas1.ActualWidth + fe.ActualWidth + 20;
            dakf.KeyFrames[1].KeyTime = TimeSpan.FromSeconds(App.Duration);
            Storyboard.SetTarget(sbRun.Children[0], fe);
            sbRun.Completed += (s, e) =>
            {
                try
                {
                    var mTContainer = framework.Assigner.Elements[fe];
                    framework.UnregisterElement(mTContainer.Id);
                    dicSb.Remove(fe);
                    canvas1.Children.Remove(fe);
                }
                catch (Exception)
                {
                }

            };
            sbRun.Begin(fe, isControllable: true);
            dicSb[fe] = sbRun;
        }



        private void ClearAll()
        {
            framework.UnregisterAllElements();
            RemovePoints();
            for (int i = 1; i < canvas1.Children.Count; i++)
            {
                canvas1.Children.RemoveAt(i);
            }
            DisplayPoints();
            numItems = 0;
        }

        private FrameworkElement AddPhoto(string filePath)
        {
            Photo p = new Photo();
            System.Windows.Controls.Image i = p.SetPicture(filePath, 800);
            RenderOptions.SetBitmapScalingMode(i, BitmapScalingMode.HighQuality);
            ElementProperties prop = new ElementProperties();
            prop.ElementSupport.AddSupportForAll();
            MTContainer cont = new MTSmoothContainer(p, canvas1, prop);
            framework.RegisterElement(cont);
            cont.MaxX = (int)canvas1.ActualWidth;
            cont.MaxY = (int)canvas1.ActualHeight;
            cont.MinX = (int)(canvas1.ActualWidth / 10.0);
            cont.MinY = (int)(canvas1.ActualHeight / 10.0);
            return p;
        }

        private void DelayedRotate(int delayMs)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += NonBlockingDelay;
            bw.RunWorkerCompleted += DelayedRotateCallback;
            bw.RunWorkerAsync(delayMs);
        }

        private void NonBlockingDelay(object sender, DoWorkEventArgs e)
        {
            int delay = (int)e.Argument;
            Thread.Sleep(delay);
        }

        private void DelayedRotateCallback(object sender, RunWorkerCompletedEventArgs e)
        {
            RotateAll();
        }

        private void PosAll()
        {
            foreach (MTContainer cont in framework.Assigner.Elements.Values)
            {
                int difX = (canvas1.ActualWidth > 200.0) ? 200 : 0;
                int difY = (canvas1.ActualWidth > 200.0) ? 200 : 0;
                int x = randomGen.Next(0, (int)canvas1.ActualWidth - difX);
                int y = randomGen.Next(0, (int)canvas1.ActualHeight - difY);
                Canvas.SetTop(cont.WorkingObject, y);
                Canvas.SetLeft(cont.WorkingObject, x);
                cont.Reset();
                cont.StartX = x;
                cont.StartY = y;
            }
        }

        private void MoveAll()
        {
            foreach (MTContainer cont in framework.Assigner.Elements.Values)
            {
                int difX = (canvas1.ActualWidth > 200.0) ? 200 : 0;
                int difY = (canvas1.ActualWidth > 200.0) ? 200 : 0;
                int x = randomGen.Next(0, (int)canvas1.ActualWidth - difX);
                int y = randomGen.Next(0, (int)canvas1.ActualHeight - difY);
                Rect curPos = cont.GetElementBounds();
                x -= (int)curPos.Left;
                y -= (int)curPos.Top;
                cont.Move(x, y);
            }
        }

        private void RotateAll()
        {
            foreach (MTContainer cont in framework.Assigner.Elements.Values)
            {
                int a = randomGen.Next(-90, 90);
                PointF p = cont.GetElementCenter();
                cont.Rotate(a, p);
            }
        }

        private void RotateAll(int angle)
        {
            foreach (MTContainer cont in framework.Assigner.Elements.Values)
            {
                PointF p = cont.GetElementCenter();
                cont.Rotate(angle, p);
            }
        }

        private FrameworkElement AddVideo(string filePath)
        {
            VideoControl p = new VideoControl();
            System.Windows.Shapes.Rectangle i = p.SetVideo(filePath);
            ElementProperties prop = new ElementProperties();
            prop.ElementSupport.AddSupportForAll();
            MTContainer cont = new MTSmoothContainer(p, canvas1, prop);
            framework.RegisterElement(cont);
            cont.MaxX = (int)canvas1.ActualWidth;
            cont.MaxY = (int)canvas1.ActualHeight;
            cont.MinX = (int)(canvas1.ActualWidth / 10.0);
            cont.MinY = (int)(canvas1.ActualHeight / 10.0);
            return p;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.B)
            {
                takeBackground();
            }
            else if (e.Key == Key.R)
            {
                ClearAll();
                LoadFolders();
                PosAll();
            }
            else if (e.Key == Key.Space)
            {
                PosAll();
            }
            else if (e.Key == Key.S)
            {
                RotateAll(30);
            }
            else if (e.Key != Key.W && e.Key == Key.Return)
            {
                toggleFullscreen();
            }
        }

        private void takeBackground()
        {
            framework.ForceRefresh();
        }

        private void toggleFullscreen()
        {
            if (!fullscreen)
            {
                switchFullScreen();
            }
            else
            {
                switchWindowed();
            }
        }

        private void switchWindowed()
        {
            base.WindowState = WindowState.Normal;
            base.WindowStyle = WindowStyle.SingleBorderWindow;
            base.Left = window_left;
            base.Top = window_top;
            base.Width = window_width;
            base.Height = window_height;
            fullscreen = false;
        }

        private void switchFullScreen()
        {
            window_left = base.Left;
            window_top = base.Top;
            window_width = base.Width;
            window_height = base.Height;
            base.Left = 0.0;
            base.Top = 0.0;
            base.Width = screen_width;
            base.Height = screen_height;
            base.ResizeMode = ResizeMode.NoResize;
            base.WindowStyle = WindowStyle.None;
            fullscreen = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            PlayOrStopMusic();
            ClearAll();
            framework.Stop();
        }

        public Rect SurfaceArea = Rect.Empty;

        void switchFullScreen(double resolutionX, double resolutionY)
        {
            ProjectionConfig proj = ProjectionConfig.Load(AppConfig.TrackingPath);
            if (proj == null)
            {
                proj = ProjectionConfig.LoadDefaults();
            }
            SurfaceArea = GetScreenDimensions(proj.OffsetX,
                proj.OffsetY,
                proj.ScaleX,
                proj.ScaleY,
                resolutionX,
                resolutionY);
            this.Width = SurfaceArea.Width;
            this.Height = SurfaceArea.Height;
            this.Top = SurfaceArea.Top;
            this.Left = SurfaceArea.Left;
            Console.WriteLine(" w :{0} h:{1}", Width, Height);
        }

        public Rect GetScreenDimensions(float offsetX, float offsetY, float scaleX, float scaleY, double fullscreenWidth, double fullscreenHeight)
        {
            // Screen is scaled by an inverse value, so calculate the screen pixel dimensions
            int sW = (int)(fullscreenWidth / scaleX);
            int sH = (int)(fullscreenHeight / scaleY);

            // Screen offset is a value from -1 to +1 so convert to 0 to 1 and base it on the center
            float offsetX2d = 0.5f - (offsetX / 2f + 0.5f);
            float offsetY2d = 0.5f - (offsetY / 2f + 0.5f);

            // Calculate the pixel offset by the full screen resolution
            int offsetPixX = (int)(offsetX2d * fullscreenWidth);
            int offsetPixY = (int)(offsetY2d * fullscreenHeight);

            // Offset the windows based on the center and set the window dimensions
            double left = (fullscreenWidth / 2) - (sW / 2) - offsetPixX;
            double top = (fullscreenHeight / 2) - (sH / 2) - offsetPixY;

            // Return the dimensions
            Rect r = new Rect(left, top, sW, sH);
            return r;
        }
    }
}
