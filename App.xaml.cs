using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows;

namespace FilePlayer
{
	public partial class App : Application
	{
		public static string ImgDirectory = Path.GetFullPath(ConfigurationManager.AppSettings["ImgDirectory"]);

		public static double Interval = Convert.ToDouble(ConfigurationManager.AppSettings["Interval"]);
		public static double Duration = Convert.ToDouble(ConfigurationManager.AppSettings["Duration"]);



		protected override void OnStartup(StartupEventArgs e)
		{
			if (!Directory.Exists(ImgDirectory))
			{
				MessageBox.Show("请配置正确的 文件路径,在.Config文件 的ImgDirectory参数");
				try
				{
					string str = Process.GetCurrentProcess().ProcessName + ".exe.config";
					Process.Start("notepad", str);
				}
				catch
				{
				}
				Environment.Exit(0);
			}
			base.OnStartup(e);
		} 
	  
	}
}
