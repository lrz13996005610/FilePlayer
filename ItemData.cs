namespace FilePlayer
{
	public class ItemData
	{
		private int _id;

		private string _name;

		private int _fileType;

		public int Id
		{
			get
			{
				return _id;
			}
			set
			{
				if (_id != value)
				{
					_id = value;
				}
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				if (_name != value)
				{
					_name = value;
				}
			}
		}

		public int FileType
		{
			get
			{
				return _fileType;
			}
			set
			{
				if (_fileType != value)
				{
					_fileType = value;
				}
			}
		}
	}
}
