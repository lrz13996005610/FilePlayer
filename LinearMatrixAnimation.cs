using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace FilePlayer
{
	public class LinearMatrixAnimation : AnimationTimeline
	{
		public static DependencyProperty FromProperty = DependencyProperty.Register("From", typeof(Matrix?), typeof(LinearMatrixAnimation), new PropertyMetadata(null));

		public static DependencyProperty ToProperty = DependencyProperty.Register("To", typeof(Matrix?), typeof(LinearMatrixAnimation), new PropertyMetadata(null));

		public Matrix? From
		{
			get
			{
				return (Matrix)GetValue(FromProperty);
			}
			set
			{
				SetValue(FromProperty, value);
			}
		}

		public Matrix? To
		{
			get
			{
				return (Matrix)GetValue(ToProperty);
			}
			set
			{
				SetValue(ToProperty, value);
			}
		}

		public override Type TargetPropertyType => typeof(Matrix);

		public LinearMatrixAnimation()
		{
		}

		public LinearMatrixAnimation(Matrix from, Matrix to, Duration duration)
		{
			base.Duration = duration;
			From = from;
			To = to;
		}

		public override object GetCurrentValue(object defaultOriginValue, object defaultDestinationValue, AnimationClock animationClock)
		{
			if (!animationClock.CurrentProgress.HasValue)
			{
				return null;
			}
			double progress = animationClock.CurrentProgress.Value;
			Matrix from = From ?? ((Matrix)defaultOriginValue);
			if (To.HasValue)
			{
				Matrix to = To.Value;
				Matrix newMatrix = new Matrix((to.M11 - from.M11) * progress + from.M11, 0.0, 0.0, (to.M22 - from.M22) * progress + from.M22, (to.OffsetX - from.OffsetX) * progress + from.OffsetX, (to.OffsetY - from.OffsetY) * progress + from.OffsetY);
				return newMatrix;
			}
			return Matrix.Identity;
		}

		protected override Freezable CreateInstanceCore()
		{
			return new LinearMatrixAnimation();
		}
	}
}
